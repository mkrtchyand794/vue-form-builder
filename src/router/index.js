import Vue from "vue";
import VueRouter from "vue-router";
import isAuth from "../middleware/auth";
import store from "../store";
import isGuest from "../middleware/guest";

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Authorization"),
    meta: {
      middleware: [isGuest],
    },
  },
  {
    path: "/new-scheme",
    name: "NewSchema",
    component: () => import("../views/CreateSchema"),
    meta: {
      middleware: [isAuth],
    },
  },
  {
    path: "/",
    name: "Schemas",
    component: () => import("../views/schema/index.vue"),
    meta: {
      middleware: [isAuth],
    },
  },
  {
    path: "/schema/:id",
    name: "Schema",
    component: () => import("../views/schema/_id.vue"),
    meta: {
      middleware: [isAuth],
    },
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

router.beforeEach((to, from, next) => {
  if (!to.meta.middleware) {
    return next();
  }
  const middleware = to.meta.middleware;
  const context = {
    to,
    from,
    next,
    store,
  };
  return middleware[0]({
    ...context,
  });
});

export default router;
