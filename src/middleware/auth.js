export default function isAuth({ next, store }) {
  if (store.state.auth.user) {
    return next();
  }
  return next({ name: "Login" });
}
