export default function isGuest({ next, store }) {
  if (!store.state.auth.user) {
    return next();
  }
  return next({ name: "Schema" });
}
