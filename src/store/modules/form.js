import axios from "axios";

const createField = () => {
  return {
    id: new Date().getTime(),
    key: {
      value: "",
      isRequired: true,
      error: false,
    },
    label: {
      value: "",
      isRequired: true,
      error: false,
    },
    type: {
      value: "",
      isRequired: true,
      error: false,
    },
  };
};

export default {
  namespaced: true,
  state: {
    schemas: [],
    newSchema: {
      name: {
        value: "",
        isRequired: true,
        error: false,
      },
      fields: [createField()],
    },
  },
  getters: {},
  mutations: {
    addField(state) {
      state.newSchema.fields.push(createField());
    },
    fillField(state, { id, field }) {
      // eslint-disable-next-line no-unused-vars
      const currentIndex = state.newSchema.fields.findIndex(
        (sField) => sField.id === id
      );
      state.newSchema.fields[currentIndex] = {
        ...state.newSchema.fields[currentIndex],
        ...field,
      };
    },
    setSchemaName(state, name) {
      state.newSchema.name.value = name;
    },
    setFieldValidation(state, { id, validation }) {
      const currentField = state.newSchema.fields.find(
        (field) => field.id === id
      );
      currentField.validation = validation;
    },
    setFieldOptions(state, { id, options }) {
      const currentField = state.newSchema.fields.find(
        (field) => field.id === id
      );
      currentField.options = options;
    },
    deleteField(state, id) {
      state.newSchema.fields = state.newSchema.fields.filter(
        (field) => field.id !== id
      );
    },
  },
  actions: {
    async fetchSchemas() {
      return await axios.get("form");
    },
    async fetchSchema(_, id) {
      return await axios.get(`form/${id}`);
    },
    async addSchema({ state }) {
      try {
        const schema = {
          ...state.newSchema,
          name: state.newSchema.name.value,
          fields: state.newSchema.fields.map((field) => ({
            key: field.key.value,
            type: field.type.value,
            label: field.label.value,
            validation: field.validation,
            options: field.options,
          })),
        };
        return await axios.post("form", { schema });
      } catch (e) {
        console.log(e);
      }
    },
  },
};
