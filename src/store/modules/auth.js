import axios from "axios";

export default {
  namespaced: true,
  state: {
    token: null,
    user: null,
  },
  getters: {
    user(state) {
      return state.user;
    },
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
      if (token) {
        localStorage.setItem("token", token);
      } else {
        localStorage.removeItem("token");
      }
    },
    setUser(state, user) {
      state.user = user;
    },
  },
  actions: {
    async signIn({ dispatch }, data) {
      await axios.post("auth/login", data).then((res) => {
        return dispatch("attempt", res.data.access_token);
      });
    },

    async attempt({ commit, state }, token) {
      if (token) {
        commit("setToken", token);
      }

      if (!state.token) {
        return;
      }
      axios.defaults.headers.common = {
        Authorization: "Bearer " + token,
      };
      await axios
        .get("users/profile")
        .then((user) => {
          console.log(user, "user");
          commit("setUser", user.data);
        })
        .catch(() => {
          commit("setToken", null);
        });
    },

    signOut: ({ commit }) => {
      commit("setToken", null);
      commit("setUser", null);
    },
  },
};
