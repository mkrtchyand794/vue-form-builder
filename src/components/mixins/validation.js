export const validationMixin = {
  methods: {
    checkFields(fields, res) {
      fields.forEach((field) => {
        this.checkIsEmptyFields(field, res);
      });
    },
    checkIsEmptyFields(form, res) {
      Object.values(form).forEach((field) => {
        if (typeof field === "object" && field.isRequired) {
          this.setValue(!field.value, field, res);
        }
      });
    },
    checkSingleField(field, res) {
      if (field.isRequired) {
        this.setValue(!field.value, field, res);
      }
    },
    checkMinMax(field, res) {
      const valueAsNumber =
        field.type === "number" ? Number(field.value) : field.value.length;
      if (field.validation.min) {
        this.setValue(valueAsNumber < field.validation.min, field, res);
      }
      if (!field.error && field.validation.max) {
        this.setValue(valueAsNumber > field.validation.max, field, res);
      }
    },
    setValue(wrongCondition, field, res) {
      if (wrongCondition) {
        field.error = true;
        res.status = false;
      } else {
        field.error = false;
      }
    },
  },
};
